# LED Sat Tracker

To update the code:

```shell
pip install adafruit-ampy

ampy -p /dev/XXXXX put main.py
```

To access the console (picocom needed):

```shell
picocom /dev/XXXX -b 115200
```

To stop the code in the console and prevent rebooting from watchdog (may be needed to reflash the software):
`<ctrl> + C` to hit the python REPL then quickly enter `rm()` before the watchdog triggers. It will delete the `main.py` and allow to re-upload it safely. 

