import json
import time

import esp
from machine import RTC
import ntptime_ as ntptime
import machine
import utime
from machine import Pin

from wdt import wdt_feed

# machine.disable_irq()
# machine.enable_irq(32)
esp.osdebug(None)
print('Reset cause: {}'.format(machine.reset_cause()))

# CONSTANTS

# GPS Coordinates (Paris)
LATITUDE = 48.8534
LONGITUDE = 2.3488
NORAD_ID = 25544  # ISS
WIFI_SSID = 'At-Home-PRIVE'
WIFI_PASSWORD = 'g93c7qHW'

# / CONSTANTS

p14 = Pin(14, Pin.OUT)
p14.off()

def do_connect ():
    import network

    sta_if = network.WLAN(network.STA_IF)
    print('Connecting to network...', end = '')
    wdt_feed()

    if not sta_if.isconnected():
        sta_if.active(True)
        sta_if.connect(WIFI_SSID, WIFI_PASSWORD)
        while not sta_if.isconnected():
            wdt_feed()
            time.sleep(0.5)

    print('done.')

def http_get (url):
    import socket

    wdt_feed()
    _, _, host, path = url.split('/', 3)
    addr = socket.getaddrinfo(host, 80)[0][-1]
    wdt_feed()
    s = socket.socket()
    s.connect(addr)
    wdt_feed()
    s.send(bytes('GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n' % (path, host), 'utf8'))
    wdt_feed()
    returned_data = ''
    while True:
        data = s.recv(100)
        wdt_feed()
        if data:
            returned_data += str(data, 'utf8')
        else:
            break
    wdt_feed()

    s.close()

    return returned_data

def real_timestamp ():
    return utime.time() + 946684800

do_connect()

# Time
rtc = RTC()
rtc.datetime((2017, 8, 23, 1, 12, 48, 0, 0))  # set a specific date and time
print(rtc.datetime())  # get date and time

# synchronize with ntp
# need to be connected to wifi
wdt_feed()
ntptime.settime()  # set the rtc datetime from the remote server
print(rtc.datetime())  # get the date and time in UTC
wdt_feed()

i = 1

while 1:
    wdt_feed()
    print(i)

    if i % 3600 == 0:
        i = 0
        print('Updating time..')
        try:
            ntptime.settime()
        except OSError:
            wdt_feed()
            print('Could not get time')
            continue
    else:
        i += 1

    wdt_feed()
    print('Fetching next pass..')

    response = http_get('http://satellites.fly.dev/passes/{}?lat={}&lon={}&limit=1'.format(NORAD_ID, LATITUDE, LONGITUDE))
    wdt_feed()

    passe = json.loads(response.split('\n')[-1])[0]

    alt = float(passe['culmination']['alt'])
    print('Next passe altitude: {}°'.format(alt))

    wdt_feed()

    print('Passe time  : {} (timestamp: {})'.format(passe['culmination']['utc_datetime'], passe['culmination']['utc_timestamp']))
    # (2020, 12, 13,   6,      12,    27,      17, 531)
    year, month, day, _, hour, minute, second, *_ = rtc.datetime()
    print('Current time: {}-{}-{} {}:{}:{}              (timestamp: {})'.format(year, month, day, hour, minute, second, real_timestamp()))

    wdt_feed()
    current_timestamp = real_timestamp()
    wdt_feed()
    passe_timestamp_start = passe['rise']['utc_timestamp']
    passe_timestamp_end = passe['set']['utc_timestamp']
    wdt_feed()
    if (alt >= 45 and alt <= 135 and current_timestamp >= passe_timestamp_start and current_timestamp <= passe_timestamp_end):
        # It's over your head
        print('LED ON.')
        p14.on()
    else:
        p14.off()

    print()
    wdt_feed()

    for _ in range(15):
        time.sleep(0.5)
        wdt_feed()
