from machine import Pin

p14 = Pin(14, Pin.OUT)
p14.off()

import esp
import uos

esp.osdebug(None)
import gc

# import webrepl
# webrepl.start()
gc.collect()

def rm ():
    uos.remove('main.py')

import main
