from machine import WDT

WDT_ENABLED = True

if WDT_ENABLED:
    wdt = WDT()

def wdt_feed ():
    global WDT_ENABLED
    global wdt

    if WDT_ENABLED:
        wdt.feed()
